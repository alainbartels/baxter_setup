#!/bin/bash

## Install Ros Lunar ##
#Setup your sources.list
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
#Setup keys
sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116

#Verify latest Debians
apt-get update

apt-get install 
#Initialize rosdep

rosdep init
rosdep fix-permissions
rosdep update

#Install ROS Lunar Desktop Full
apt-get install -y ros-lunar-desktop-full



#Install rosinstall
apt-get install -y python-ros-install

## Create Baxter Development Workspace ##
#Create ROS Workspace
mkdir -p ~/ros_ws/src

#Source ROS and build
source /opt/ros/lunar/setup.bash && \
        cd ~/ros_ws && \
        catkin_make && \
        catkin_make install

## Install baxter SDK Dependencies
apt-get update && apt-get install -y \
        git-core \
        nano \
        python-argparse \
        python-wstool \
        python-vcstools \
        python-rosdep \
        python-rosinstall \
        ros-lunar-geographic-msgs \
        ros-lunar-control-msgs \
        ros-lunar-joystick-drivers \
        python3 \
        python \
        wget
## Install Baxter Research Robot SDK ##
#Install baxter SDK
cd ~/ros_ws/src && \
        wstool init . && \
        wstool merge https://raw.githubusercontent.com/RethinkRobotics/baxter/master/baxter_sdk.rosinstall && \
        wstool update

#Source ROS Setup
source /opt/ros/lunar/setup.bash && \
        cd ~/ros_ws && \
        catkin_make && \
        catkin_make install

## Step 6: Configure Baxter Communication/ROS Workspace ##
cd ~/ros_ws && \
        wget --no-check-certificate https://github.com/RethinkRobotics/baxter/raw/master/baxter.sh && \
        chmod u+x baxter.sh
